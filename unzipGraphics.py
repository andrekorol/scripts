#!usr/bin/env python3

import os
import subprocess

path = os.getcwd()

for dirpath, dirnames, filenames in os.walk(path):
    subprocess.call("cd " + dirpath + " && jar xf graphics.zip && rm graphics.zip", shell=True)
