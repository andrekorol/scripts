#!/usr/bin/env python3.6
import os

path = os.getcwd()
makeFile = "CMakeLists.txt"
open(makeFile, 'w').close()
fin = open(makeFile, 'w+')
files = 0
fin.write("add_library(\n")
for dirpath, dirnames, filenames in os.walk(path):
    for name in filenames:
        if name[-4:] == ".cpp" or name[-2:] == ".h" or name[-2:] == ".c":
            fin.write("\t".expandtabs(4) + name + "\n")

fin.close()

with open(makeFile, 'rb+') as fileHandle:
    fileHandle.seek(-1, os.SEEK_END)
    fileHandle.truncate()
    fileHandle.close()

with open(makeFile, 'a') as fileHandle:
    fileHandle.write(")\n")
    fileHandle.close()
