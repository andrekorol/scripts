#!/usr/bin/env python3.6
import os
import sys
from chardet.universaldetector import UniversalDetector

filename, ext, string_to_delete = sys.argv


def remove_string_from_line(ext, string_to_delete):
    """Removes all occurrences of a certain string from all files in the present
     directory with the desired extension.

    Keyword arguments:
    ext -- file(s) extension (e.g. .c)
    string_to_delete -- string to be removed from file(s)
    (e.g. #include <conio.h>)
    """

    for filename in os.listdir('.'):
        if filename.endswith(ext):
            infile = filename
            outfile = 'new_' + filename

            delete_list = [string_to_delete]

            detector = UniversalDetector()
            detector.reset()
            for line in open(filename, 'rb'):
                    detector.feed(line)
                    if detector.done:
                        break
            detector.close()
            code = detector.result

            fin = open(filename, "r", -1, code['encoding'])
            fout = open(outfile, "w+")
            for line in fin:
                if string_to_delete not in line:
                    fout.write(line)
            fin.close()
            fout.close()
            os.remove(filename)
            os.rename(outfile, filename)

remove_string_from_line(ext, string_to_delete)
