#!/usr/bin/env python3.6
import os

path = os.getcwd()
file_list = []
duplicates = {}

# remove filename duplicates
for dirpath, dirnames, filenames in os.walk(path):
    for name in filenames:
        if name not in file_list:
            file_list.append(name)
        else:
            if name not in duplicates.keys():
                duplicates[name] = 1
                new_name = str(duplicates[name]) + '_' + name
                os.replace(os.path.join(dirpath, name), os.path.join(dirpath, new_name))
            else:
                duplicates[name] += 1
                new_name = str(duplicates[name]) + '_' + name
                os.replace(os.path.join(dirpath, name), os.path.join(dirpath, new_name))
