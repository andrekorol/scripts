#!/usr/bin/env python3
import os
import platform
import argparse
import sys

# First of all, we need to get both the complete user's input and the arguments and assign these values to variables
executable = sys.argv[0].split('/')[-1]
arguments = ' '.join(sys.argv[1:])
userInput = executable + ' ' + arguments

# Next, we need to define our Exceptions


class Error(Exception):
    """Base class for exceptions in this module."""

    pass


class COrCppFileNotFoundError(Error):
    """Exception raised when no C or C++ files are found under the current directory

        Attributes:
            expression -- input expression in which the error occurred
            message -- explanation of the error
    """

    def __init__(self, expression, message):
        self.expression = expression
        self.message = message


class InvalidArgumentError(Error):
    """Exception raised when an invalid argument is given as input

        Attributes:
            expression -- input expression in which the error occurred
            message -- explanation of the error
    """

    def __init__(self, expression, message):
        self.expression = expression
        self.message = message


class MissingArgumentError(Error):
    """Exception raised when there are missing argument(s)

        Attributes:
            expression -- input expression in which the error occurred
            message -- explanation of the error
    """
    def __init__(self, expression, message):
        self.expression = expression
        self.message = message


# And then, we get our current directory and assign it to the 'path' variable
path = os.getcwd()

# Now, we fill the 'blacklist' with the entrances already in 'blacklist.txt'(if there are any)
# We first check if blacklist.txt already exists in the current directory.
# If it's missing, we create it as a blank file
filename = "blacklist.txt"
if not os.path.isfile(filename):
    open(filename, 'w').close()

# After checking if the file exists we remove any existing blank lines(if there are any)
open("new" + filename, 'w').close()
with open(filename) as fin, open("new" + filename, 'w') as fOut:
    for line in fin:
        if not line.strip():
            continue
        fOut.write(line)
os.remove(filename)
os.rename("new" + filename, filename)

# And then we can proceed to adding the lines in the file blacklist.txt to a list called blacklist
# If the file is empty, we simply create a blank list to be filled later
blacklist = [line.rstrip('\n') for line in open(filename)]

# Now we're going to setup the Argument Parser for this program
parser = argparse.ArgumentParser(description="Update or create CMakeLists.txt on the current directory.")
parser.add_argument('option', metavar='option', type=str, nargs='?',
                    help='Run just "updateMakeLists.py"(no quotes) if you just want to create or update '
                         'CMakeLists.txt.\n If you want to exclude files from CMakeLists.txt enter '
                         '"updateMakeLists.py rm files"(no quotes)\n (e.g., updateMakeLists.py rm '
                         'CMakeCXXCompilerId.cpp feature_tests.c). This command will create a file named '
                         'blacklist.txt(if it does not already exists) and will write the files you gave as '
                         'arguments to it. By doing so, these files will be removed from CMakeLists.txt, if they are '
                         'already in there, otherwise they will just not be added to it. If you want the files you '
                         'excluded from CMakeLists.txt by using the "rm" argument back into it you will have to add '
                         'them manually or you can use the "add" argument followed by the file-names. '
                         '(e.g., updateMakeLists.py add main.cpp matrix.cpp)')
parser.add_argument('files', type=str, nargs='*',
                    help='If you used the "rm" argument, follow it by the name(s) of the file(s) that you '
                         'want to exclude from CMakeLists.txt \n(e.g., updateMakeLists.py rm '
                         'CMakeCXXCompilerId.cpp feature_tests.c). And if you used the "add" argument follow it by '
                         'the name(s) of the file(s) that you want back into CMakeLists.txt (e.g., '
                         'updateMakeLists.py add main.cpp matrix.cpp)')

args = parser.parse_args()

# If the option argument was given but no files argument(s) were given, we print the Argument Parser help
#  and raise an InvalidArgumentError
validArguments = ["add", "rm"]


def files_validation(files):
    """

    :type files: list
    """
    for f in files:
        if f[-2:] != '.c' and f[-4:] != '.cpp':
            return True
    return False


# Now, if any of the arguments is invalid, we print the Argument Parser help and raise an InvalidArgumentError
if args.option is not None and (args.option not in validArguments or files_validation(args.files)):
    parser.print_help()
    InvalidArgument = InvalidArgumentError(arguments, "Invalid(s) argument(s) were given. Please read the help above "
                                                      "or enter 'updateMakeLists.py -h' or 'updateMakeLists.py --help' "
                                                      "to read it again")
    raise InvalidArgument
# And if no argument is given for 'files', we print the Argument Parser help and raise an MissingArgumentError
elif not args.files and args.option is not None:
    parser.print_help()
    MissingArgument = MissingArgumentError(arguments, "Missing 'files' argument(s). Please read the help above "
                                                      "or enter 'updateMakeLists.py -h' or 'updateMakeLists.py --help' "
                                                      "to read it again")
    raise MissingArgument


# If the 'option' argument used was "rm", we add the files used as arguments to the blacklist
if args.option == "rm":
    for file in args.files:
        blacklist.append(file)
# And if the 'option' argument used was "add" we remove the files used as arguments from the blacklist
elif args.option == "add":
    for file in args.files:
        try:
            blacklist.remove(file)
        except ValueError:
            pass


# Before updating blacklist.txt, we remove all possible duplicates in the blacklist
new_blacklist = []
for n in blacklist:
    if n not in new_blacklist:
        new_blacklist.append(n)
blacklist = new_blacklist

# Now we can update blacklist.txt by adding the files passed as arguments
with open(filename, 'w+') as fin:
    for file in blacklist:
        fin.write(file + '\n')

# We need to now check whether blacklist is empty or not. If it's indeed empty, we remove blacklist.txt
if not blacklist:
    os.remove(filename)

# Begin of the creation and edition of the file CMakeLists.txt"
filename = "CMakeLists.txt"
open(filename, 'w').close()
fin = open(filename, "w+")
fin.write("cmake_minimum_required(VERSION 3.7)\n")
COrCppFound = False
for dirpath, dirnames, filenames in os.walk(path):
    for name in filenames:
        if platform.system() == "Windows":
            dirpath = dirpath.replace('\\', '/')
        if name[-2:] == ".c":
            if name not in blacklist:
                COrCppFound = True
                fin.write("add_executable (" + name[:-2] + " " + dirpath + "/" + name + ")\n")
        elif name[-4:] == ".cpp":
            if name not in blacklist:
                COrCppFound = True
                fin.write("add_executable (" + name[:-4] + " " + dirpath + "/" + name + ")\n")

fin.close()

# Now we check if C or C++ files were found(excluding the ones on the blacklist)
if not COrCppFound and not blacklist:
    fileError = COrCppFileNotFoundError(userInput, "No C(.c) or C++(.cpp) files were found "
                                                   "in the current directory:" + path + " "
                                                   "Please make sure to run this program on a "
                                                   "directory that contains C or C++ source files. "
                                                   "Accepted extensions are: .c for C source files and .cpp for C++ "
                                                   "source files")
    raise fileError
